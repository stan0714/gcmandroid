package com.zxing.activity;

import java.io.IOException;
import java.util.Vector;

import org.json.JSONObject;



import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.example.test.MainActivity;
import com.example.test.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.zxing.camera.CameraManager;
import com.zxing.decoding.CaptureActivityHandler;
import com.zxing.decoding.InactivityTimer;
import com.zxing.view.ViewfinderView;
/**
 * Initial the camera
 * @author Ryan.Tang
 */
public class CaptureActivity extends Activity implements Callback {

	private CaptureActivityHandler handler;
	private ViewfinderView viewfinderView;
	private boolean hasSurface;
	private Vector<BarcodeFormat> decodeFormats;
	private String characterSet;
	private InactivityTimer inactivityTimer;
	private MediaPlayer mediaPlayer;
	private boolean playBeep;
	private static final float BEEP_VOLUME = 0.10f;
	private boolean vibrate;
	private Button cancelScanButton;
	String tag="QRCODE";
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.camera);
		//ViewUtil.addTopView(getApplicationContext(), this, R.string.scan_card);
		CameraManager.init(getApplication());
		viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
		cancelScanButton = (Button) this.findViewById(R.id.btn_cancel_scan);
		hasSurface = false;
		inactivityTimer = new InactivityTimer(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
		SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface) {
			initCamera(surfaceHolder);
		} else {
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		decodeFormats = null;
		characterSet = null;

		playBeep = true;
		AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
		if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
			playBeep = false;
		}
		initBeepSound();
		vibrate = true;
		
		//quit the scan view
		cancelScanButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CaptureActivity.this.finish();
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (handler != null) {
			handler.quitSynchronously();
			handler = null;
		}
		CameraManager.get().closeDriver();
	}

	@Override
	protected void onDestroy() {
		inactivityTimer.shutdown();
		super.onDestroy();
	}
	
	/**
	 * Handler scan result
	 * @param result
	 * @param barcode
	 */
	public void handleDecode(Result result, Bitmap barcode) {
		inactivityTimer.onActivity();
		playBeepSoundAndVibrate();
		String resultString = result.getText();
		//FIXME
		if (resultString.equals("")) {
			Toast.makeText(CaptureActivity.this, "Scan failed!", Toast.LENGTH_SHORT).show();
		}else {
			//sendtableActivity sendtableActivity=new sendtableActivity();
			//boolean connected=sendtableActivity.isInternetConnected();
			//Log.i(tag, "Connect:"+connected);
			//Toast.makeText(CaptureActivity.this, "BARCODE is:"+resultString, Toast.LENGTH_SHORT).show();
			Log.i(tag, resultString);
			checkQRcodeResult(resultString);
			if(isInternetConnected()){
				Log.i(tag, resultString);
				//Toast.makeText(CaptureActivity.this, "Scan result:"+resultString, Toast.LENGTH_SHORT).show();
				//checkQRcodeResult(resultString);
			}else{
//				SQLite sqLite=new SQLite(this);
//				sqLite.InsertQrocde(resultString,  storetableActivity.account);
//				Toast.makeText(CaptureActivity.this,(String)getResources().getText(R.string.CouponUnSendMessage), Toast.LENGTH_SHORT).show();		
			}
		}
		//CaptureActivity.this.finish();
	}
//	//重送 QRCODE掃近來的QRCODE
//	public void resendQRcodeResult(String result){
//		HttpTool tool=new HttpTool();
//		String msg;
//		//SQLite sqLite=new SQLite(this);
//		//sqLite.deleteQrcode(result, mainActivity.username);
//		switch (result.length()) {
//		case 20:
//			//sqLite.deleteQrcode(result, mainActivity.username);
//			tool.savePromoteCoupon(HttpTool.url, result, storetableActivity.account, storetableActivity.password);
//			//長度20碼微推廣
//			break;
//		case 22:
//			//sqLite.deleteQrcode(result, mainActivity.username);
//			msg=tool.saveCoupon(HttpTool.url, result, storetableActivity.account, storetableActivity.password);
//			break;
//			
//		default:
//			break;
//		}
//
//		
//	}
	
	//判斷QRCODE長度 依照長度判定
	public void checkQRcodeResult(String result){
		
//		//HttpTool tool=new HttpTool();
//		String msg;
//		//SQLite sqLite=new SQLite(this);
//		switch (result.length()) {
//		case 20:		
//			//sqLite.InsertQrocde(result, mainActivity.username);
//			//msg=tool.savePromoteCoupon(HttpTool.url, result, storetableActivity.account, storetableActivity.password);
//			Log.i(tag,"20:"+msg);
//			Qrcodemesage(msg);
//			//長度微20碼微推廣
//			break;
//		case 22:
//			//sqLite.InsertQrocde(result, mainActivity.username);
//			msg=tool.saveCoupon(HttpTool.url, result, storetableActivity.account, storetableActivity.password);
//			Qrcodemesage(msg);
//			Log.i(tag,"22:"+msg);
//			break;
		Toast.makeText(CaptureActivity.this,"BARCODE is "+result , Toast.LENGTH_SHORT).show();
		backtoMainActivityhandler.sendEmptyMessage(0);
//		default:
//			Toast.makeText(CaptureActivity.this,(String)getResources().getText(R.string.sacn_fail) , Toast.LENGTH_SHORT).show();
//			break;
//		}

		
	}
	Handler backtoMainActivityhandler = new Handler() {  
        @Override  
        public void handleMessage(Message msg) {
			Intent myIntent=new Intent(CaptureActivity.this,MainActivity.class);
			startActivityForResult(myIntent,0);				
         }  
        
    };
 
    
	
    //偵測目前系統連線狀態
	public boolean isInternetConnected(){
			ConnectivityManager cm=(ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE) ;
			NetworkInfo netinfoInfo=cm.getActiveNetworkInfo();
			if(netinfoInfo!=null&&netinfoInfo.isConnected()){
				return true;
			}else{
				return false;
			}
    	
	}
    //根據回傳的response 判定要秀在Toast上的訊息
	public void Qrcodemesage(String response){
		String msg="";
		int gate=0;
		try{
		response=new JSONObject(new JSONObject(response).getString("socialcoupon")).getString("msg").toLowerCase();
		
		Log.i(tag, "Res:"+response);
		if(response.equals("usernotfound")){
			msg=(String)getResources().getText(R.string.userNotFound);
		}else if(response.equals("discountcodenull"))
		{
			msg=(String)getResources().getText(R.string.DiscountCodeNull);
		}else if(response.equals("discountcodeerror"))
		{
			msg=(String)getResources().getText(R.string.DiscountCodeError);
		}else if(response.equals("discountused"))
		{
			msg=(String)getResources().getText(R.string.DiscountUsed);
		}else if(response.equals("couponnotfound"))
		{
			msg=(String)getResources().getText(R.string.couponNotFound);
		}else if(response.equals("discountexpired"))
		{
			msg=(String)getResources().getText(R.string.DiscountExpired);
		}else if(response.equals("discountsavefailed"))
		{
			msg=(String)getResources().getText(R.string.DiscountSaveFailed);
		}else if(response.equals("discountsaveok"))
		{
			gate=1;
			msg=(String)getResources().getText(R.string.DiscountSaveOk);
		}else if(response.equals("promotecodenull"))
		{
			msg=(String)getResources().getText(R.string.PromoteCodeNull);
		}else if(response.equals("PromoteCouponNotFound"))
		{
			msg=(String)getResources().getText(R.string.PromoteCouponNotFound);
		}else if(response.equals("couponalerdysaved"))
		{
			msg=(String)getResources().getText(R.string.CouponAlerdySaved);
		}else if(response.toLowerCase().equals("couponsaveok"))
		{
			gate=1;
			msg=(String)getResources().getText(R.string.CouponSaveOK);
		}else{
			msg=(String)getResources().getText(R.string.notdefine);
		}
		}catch (Exception e) {
			Log.i(tag, "error:"+e.getMessage());
			msg=(String)getResources().getText(R.string.notdefine);
		}
		Toast.makeText(CaptureActivity.this,msg , Toast.LENGTH_SHORT).show();
//		if(gate==0){
//			//backtomainActivityhandler.sendEmptyMessage(0);
//			backtsotreActivityhandler.sendEmptyMessage(0);
//		}else{
//			backtsotreActivityhandler.sendEmptyMessage(0);
//		}

	}
	
	

	
	
	
	private void initCamera(SurfaceHolder surfaceHolder) {
		try {
			CameraManager.get().openDriver(surfaceHolder);
		} catch (IOException ioe) {
			return;
		} catch (RuntimeException e) {
			return;
		}
		if (handler == null) {
			handler = new CaptureActivityHandler(this, decodeFormats,
					characterSet);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;

	}

	public ViewfinderView getViewfinderView() {
		return viewfinderView;
	}

	public Handler getHandler() {
		return handler;
	}

	public void drawViewfinder() {
		viewfinderView.drawViewfinder();

	}

	private void initBeepSound() {
		if (playBeep && mediaPlayer == null) {
			// The volume on STREAM_SYSTEM is not adjustable, and users found it
			// too loud,
			// so we now play on the music stream.
			setVolumeControlStream(AudioManager.STREAM_MUSIC);
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setOnCompletionListener(beepListener);

			AssetFileDescriptor file = getResources().openRawResourceFd(
					R.raw.beep);
			try {
				mediaPlayer.setDataSource(file.getFileDescriptor(),
						file.getStartOffset(), file.getLength());
				file.close();
				mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
				mediaPlayer.prepare();
			} catch (IOException e) {
				mediaPlayer = null;
			}
		}
	}

	private static final long VIBRATE_DURATION = 200L;

	private void playBeepSoundAndVibrate() {
		if (playBeep && mediaPlayer != null) {
			mediaPlayer.start();
		}
		if (vibrate) {
			Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
			vibrator.vibrate(VIBRATE_DURATION);
		}
	}

	/**
	 * When the beep has finished playing, rewind to queue up another one.
	 */
	private final OnCompletionListener beepListener = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mediaPlayer) {
			mediaPlayer.seekTo(0);
		}
	};

}