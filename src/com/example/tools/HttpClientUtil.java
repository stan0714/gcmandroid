package com.example.tools;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;


import oauth.signpost.http.HttpResponse;

import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.DefaultClientConnection;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.util.Log;



public class HttpClientUtil {
	
	private static HttpClientUtil httpClientUtil = null;
	
	public static HttpClientUtil getInstance() {
		if(httpClientUtil==null)
			httpClientUtil = new HttpClientUtil();
		return httpClientUtil;
	}
	

	private MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
	private String TAG="Http";
	private HttpClient client = new HttpClient(connectionManager);
	
	//private String separator = System.getProperty("line.separator");
	
	public HttpClientUtil() {
		
		int maxThreadsTotal = 30;
		int maxThreadsPerHost = 3;

		HttpConnectionManagerParams params = connectionManager.getParams();
		//params.setConnectionTimeout(10 * 1000);
		//params.setSoTimeout(10 * 1000);
		params.setMaxTotalConnections(maxThreadsTotal);
		if (maxThreadsTotal > maxThreadsPerHost) {
			params.setDefaultMaxConnectionsPerHost(maxThreadsPerHost);
		} else {
			params.setDefaultMaxConnectionsPerHost(maxThreadsTotal);
		}

		HostConfiguration hostConf = client.getHostConfiguration();
		ArrayList<Header> headers = new ArrayList<Header>();
		
		headers.add(new Header("Accept-Language",
				"en-us,zh-cn,zh-tw,en-gb,en;q=0.7,*;q=0.3"));
		headers.add(new Header("Accept-Charset",
				"big5,gb2312,gbk,utf-8,ISO-8859-1;q=0.7,*;q=0.7"));
		headers.add(new Header(
				"Accept",
				"text/html,text/plain,text/html,application/xml,application/json;q=0.9,application/xhtml+xml,text/xml;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5"));
		//headers.add(new Header("Content-Type","application/json;charset=utf-8"));
		headers.add(new Header("Content-Type","text/plain;charset=utf-8"));
		// headers.add(new Header("Accept-Encoding", "x-gzip, gzip"));	
		hostConf.getParams().setParameter("http.default-headers", headers);
//		hostConf.getParams().setParameter("http.protocol.content-charset", "UTF-8");
	}
	
	
	
	public String postJson(String url,JSONObject json){
		String result="";
		DefaultHttpClient httpClient=new DefaultHttpClient();
		try{
			HttpPost request=new HttpPost(url);
			StringEntity paramsEntity=new StringEntity(json.toString());
			request.addHeader("Content-type", "application/json");
			request.setEntity(paramsEntity);
			

	        result = httpClient.execute(request, new ResponseHandler<String>(){
	            @Override
				public String handleResponse(
						org.apache.http.HttpResponse response)
						throws ClientProtocolException, IOException {
					// TODO Auto-generated method stub
	                switch(response.getStatusLine().getStatusCode()){
	                case HttpStatus.SC_OK:
	                    System.out.println(HttpStatus.SC_OK);
	                    return EntityUtils.toString(response.getEntity(), "UTF-8");
	                case HttpStatus.SC_NOT_FOUND:
	                    System.out.println(HttpStatus.SC_NOT_FOUND);
	                    return "404";
	                default:
	                    System.out.println("unknown:"+response.getStatusLine().getStatusCode());
	                    return "unknown";
	                }
				}


	        });
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		return result;
	}
	
	
	public static void release(){
		if(httpClientUtil != null){
			httpClientUtil.connectionManager.shutdown();
		}
	}
	
	private HttpClient getClient() {
		return client;
	}
	
	public String getQueryString(Map<String, String> map) {

		StringBuffer sb = new StringBuffer();
		Set<String> keyset = map.keySet();
		Iterator<String> it = keyset.iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			String value = (String) map.get(key);
			sb.append("&" + key + "=" + value);
		}

		String result = sb.toString().replaceFirst("&", "");
		return result;

	}

	public String exceGet(String url, Map<String, String> map) {

		String queryString = this.getQueryString(map);
		String result = null;
		result = this.exceGet(url, queryString);
		return result;

	}

	public String exceGet(String url, String queryString) {
		
		String result = null;

		GetMethod gm = null;
		HttpClient client = this.getClient();
		try {
			gm = new GetMethod(url);
			gm.setQueryString(queryString);
			int httpstatus = client.executeMethod(gm);
			Log.i("facebook", "httpstatus:::" + httpstatus);
			
			if (httpstatus == 200) {

				BufferedReader in = new BufferedReader(
						new InputStreamReader(gm.getResponseBodyAsStream(), gm.getResponseCharSet()));
				StringBuffer buffer = new StringBuffer();
				String line = "";
				while ((line = in.readLine()) != null) {
					buffer.append(line);
				}
				result = buffer.toString();
				
			} else {
				result = "";
				Header[] headera = gm.getRequestHeaders();
			}

		} catch (Exception e) {
			Log.i(TAG, "httpstatus:::" + e.toString());
			result = "";
		} finally {
			if (gm != null) {
				gm.releaseConnection();
			}
		}

//		if (log.isInfoEnabled()) {
//			log.info("method:GET,url:" + url + ",queryString:"
//					+ queryString + ",result:" + result);
//		}
		
		return result;

	}
	
	public String excePost(String url, Map<String,String>map) {
		NameValuePair[] nvp = new  NameValuePair[ map.size()];
		Set<String> keyset = map.keySet();
		Iterator<String> it = keyset.iterator();
		int i = 0;
		while (it.hasNext()) {
			String key = (String) it.next();
			String value = (String) map.get(key);
			nvp[i] = new NameValuePair(key,value);
			i++;
		}
		return this.excePost(url, nvp);
	}
	
	public String excePost(String url, NameValuePair[] nvp) {

		String result = null;

		HttpClient client = this.getClient();

		PostMethod pm = null;

		try {

			pm = new PostMethod(url);
			pm.setRequestBody(nvp);
			int httpstatus = client.executeMethod(pm);
			if (httpstatus == 200) {

				BufferedReader in = new BufferedReader(
						new InputStreamReader(pm.getResponseBodyAsStream(), pm.getResponseCharSet()));
				StringBuffer buffer = new StringBuffer();
				String line = "";
				while ((line = in.readLine()) != null) {
					buffer.append(line);
				}
				result = buffer.toString();
				
			} else {
				result = "";
				StringBuffer queryString = new StringBuffer();
				for (int i = 0; i < nvp.length; i++) {
					queryString.append("&" + nvp[i].getName() + "="
							+ nvp[i].getValue());
				}
				Log.i(TAG, "method:POST,url:" + url + ",queryString:"+ queryString.toString() + ",httpstatus=" + httpstatus);
			}

		} catch (Exception e) {
			//System.out.println("Error:"+e.toString());
			e.printStackTrace();
			StringBuffer queryString = new StringBuffer();
			for (int i = 0; i < nvp.length; i++) {
				queryString.append("&" + nvp[i].getName() + "="
						+ nvp[i].getValue());
			}
			Log.e(TAG, ""+e.toString());
//			log.error("method:POST,url:" + url + ",queryString:"
//					+ queryString, e);

			result = "";

		} finally {
			if (pm != null) {
				pm.releaseConnection();
			}
		}

//		if (log.isInfoEnabled()) {
//			StringBuffer queryString = new StringBuffer();
//			for (int i = 0; i < nvp.length; i++) {
//				queryString.append(nvp[i].getName() + "=" + nvp[i].getValue());
//			}
//			log.info("method:POST,url:" + url + ",queryString:"
//					+ queryString + ",result:" + result);
//		}

		return result;
	}

	public String excePost(String url, String queryBody,
			String contentType, String encodeing) {

		String result = null;

		HttpClient client = this.getClient();

		PostMethod pm = null;

		try {
			pm = new PostMethod(url);
			pm.setRequestEntity(new StringRequestEntity(queryBody, contentType,
					encodeing));
			int httpstatus = client.executeMethod(pm);
			if (httpstatus == 200) {

				BufferedReader in = new BufferedReader(
						new InputStreamReader(pm.getResponseBodyAsStream(), pm.getResponseCharSet()));
				StringBuffer buffer = new StringBuffer();
				String line = "";
				while ((line = in.readLine()) != null) {
					buffer.append(line);
				}
				result = buffer.toString();
				
			} else {
				result = "";
//				log.error("method:POST,url:" + url + ",queryBody:"
//						+ queryBody + "httpstatus:" + httpstatus);
			}
		} catch (Exception e) {
			
//			log.error("method:POST,url:" + url + ",queryBody:"
//					+ queryBody, e);
			result = "";
		} finally {
			if (pm != null) {
				pm.releaseConnection();

			}
		}

//		if (log.isInfoEnabled()) {
//			log.info("method:POST,url:" + url + ",queryBody:"
//					+ queryBody + ",result:" + result);
//		}

		return result;
	}
	//2013/10/17 使用指定的encoding POST 
	public String excePost(String url,  Map<String,String>map,
			String encoding) {
		HostConfiguration hostConf = client.getHostConfiguration();
		hostConf.getParams().setParameter("http.protocol.content-charset", encoding);
		NameValuePair[] nvp = new  NameValuePair[ map.size()];
		Set<String> keyset = map.keySet();
		Iterator<String> it = keyset.iterator();
		int i = 0;
		while (it.hasNext()) {
			String key = (String) it.next();
			String value = (String) map.get(key);
			nvp[i] = new NameValuePair(key,value);
			i++;
		}
		return this.excePost(url, nvp);
	}
	
}
