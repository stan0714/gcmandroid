package com.example.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.tools.HttpClientUtil;
import com.example.tools.toolsUtil;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.zxing.activity.CaptureActivity;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	static String TAG = "YAHOO";
	GoogleCloudMessaging gcm;
	private Button yahoobutton;
	private Button zxingbutton;
	SharedPreferences prefs;
	CommonsHttpOAuthConsumer consumer = null;
	CommonsHttpOAuthProvider provider = null;
	public static final String YAHOO_CONSUMER_KEY = "dj0yJmk9RUhuVHhLWjRGUm1nJmQ9WVdrOU0yRmFabTA0Tm1zbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD02Ng--";
	public static final String YAHOO_CONSUMER_SECRET = "fe9ff1f62969ac4b25354adc3b3feef335573097";
	public static final String YAHOO_REQUEST_URL = "https://api.login.yahoo.com/oauth/v2/get_request_token";
	public static final String YAHOO_ACCESS_URL = "https://api.login.yahoo.com/oauth/v2/get_token";
	public static final String YAHOO_AUTHORIZE_URL = "https://api.login.yahoo.com/oauth/v2/request_auth";

	public static final String YAHOO_OAUTH_CALLBACK_URL = "http"
			+ "://kofman.twbbs.org/" + "restWeb/services/login/requesttoken";

	public static final String gcm_register = "http"
			+ "://www.newmusiclife.com:8080/restWeb/services/google/register";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//new GCMTask().execute();

		// yahoobutton = (Button) findViewById(R.id.yahoo);
		// yahoobutton.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		//
		//
		// consumer = new CommonsHttpOAuthConsumer(YAHOO_CONSUMER_KEY,
		// YAHOO_CONSUMER_SECRET);
		// // consumer.setMessageSigner(new HmacSha1MessageSigner());
		// provider = new CommonsHttpOAuthProvider(YAHOO_REQUEST_URL,
		// YAHOO_ACCESS_URL, YAHOO_AUTHORIZE_URL);
		// authorize();
		// //provider.setOAuth10a(true);
		// }
		// });

		zxingbutton = (Button) findViewById(R.id.take_picture);
		zxingbutton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new GCMTask().execute();
//			 GCMRegistrar.checkDevice(v.getContext());
//			 GCMRegistrar.checkManifest(v.getContext());                 
//			 String regId = GCMRegistrar.getRegistrationId(v.getContext());
//	                 if(regId.equals("")){
//	                       GCMRegistrar.register(MainActivity.this, GCMIntentService.SENDER_ID);  
//	                       
//	                 }
//	Log.i(TAG,"RegisteringC2DM onClick");
				// Intent openCameraIntent = new
				// Intent(MainActivity.this,CaptureActivity.class);
				// startActivityForResult(openCameraIntent, 0);
			}
		});
	}

	// Intent openCameraIntent = new
	// Intent(mainActivity.this,CaptureActivity.class);
	// startActivityForResult(openCameraIntent, 0);

	private void authorize() {
		new OAuthRequestTokenTask(this, consumer, provider).execute();
	}

	class OAuthRequestTokenTask extends AsyncTask<Void, Void, Void> {

		final String TAG = getClass().getName();
		private Context context;
		private OAuthProvider provider;
		private OAuthConsumer consumer;

		/**
		 * 
		 * We pass the OAuth consumer and provider.
		 * 
		 * @param context
		 *            Required to be able to start the intent to launch the
		 *            browser.
		 * @param provider
		 *            The OAuthProvider object
		 * @param consumer
		 *            The OAuthConsumer object
		 */
		public OAuthRequestTokenTask(Context context, OAuthConsumer consumer,
				OAuthProvider provider) {
			this.context = context;
			this.consumer = consumer;
			this.provider = provider;
		}

		// 06-23 15:44:19.854:
		// I/com.example.test.MainActivity$OAuthRequestTokenTask(23801):
		// Retrieving request token from servers

		/**
		 * 
		 * Retrieve the OAuth Request Token and present a browser to the user to
		 * authorize the token.
		 * 
		 */
		@Override
		protected Void doInBackground(Void... params) {

			try {
				Log.i(TAG, "Retrieving request token from servers");
				String url = provider.retrieveRequestToken(consumer,
						YAHOO_OAUTH_CALLBACK_URL);
				Log.i(TAG, "Url:" + url);
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url))
						.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
								| Intent.FLAG_ACTIVITY_NO_HISTORY
								| Intent.FLAG_FROM_BACKGROUND);
				context.startActivity(intent);
			} catch (Exception e) {
				Log.e(TAG, "Error during OAUth retrieve request token", e);
			}

			return null;
		}
	}

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		final Uri uri = intent.getData();
		// if (uri != null &&
		// uri.getScheme().equals(YAHOO_OAUTH_CALLBACK_SCHEME)) {
		Log.i(TAG, "Callback received : " + uri);
		Log.i(TAG, "Retrieving Access Token");
		new RetrieveAccessTokenTask(this, consumer, provider, prefs)
				.execute(uri);
		finish();
		// }
	}

	protected Void doInBackground(Uri... params) {
		final Uri uri = params[0];
		final String oauth_verifier = uri
				.getQueryParameter(OAuth.OAUTH_VERIFIER);

		try {
			provider.retrieveAccessToken(consumer, oauth_verifier);

			final SharedPreferences.Editor edit = prefs.edit();
			edit.putString(OAuth.OAUTH_TOKEN, consumer.getToken());
			edit.putString(OAuth.OAUTH_TOKEN_SECRET, consumer.getTokenSecret());
			edit.commit();

			String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
			String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");

			consumer.setTokenWithSecret(token, secret);
			Log.i(TAG, "OAuth - Access Token Retrieved");
			finish();

		} catch (Exception e) {
			Log.e(TAG, "OAuth - Access Token Retrieval Error", e);
		}

		return null;
	}

	public class RetrieveAccessTokenTask extends AsyncTask<Uri, Void, Void> {

		private Context context;
		private OAuthProvider provider;
		private OAuthConsumer consumer;

		public RetrieveAccessTokenTask(Context context, OAuthConsumer consumer,
				OAuthProvider provider, SharedPreferences prefs) {
			this.context = context;
			this.consumer = consumer;
			this.provider = provider;
		}

		/**
		 * Retrieve the oauth_verifier, and store the oauth and
		 * oauth_token_secret for future API calls.
		 */
		@Override
		protected Void doInBackground(Uri... params) {
			final Uri uri = params[0];
			final String oauth_verifier = uri
					.getQueryParameter(OAuth.OAUTH_VERIFIER);

			try {
				provider.retrieveAccessToken(consumer, oauth_verifier);

				final SharedPreferences.Editor edit = prefs.edit();
				edit.putString(OAuth.OAUTH_TOKEN, consumer.getToken());
				edit.putString(OAuth.OAUTH_TOKEN_SECRET,
						consumer.getTokenSecret());
				edit.commit();

				String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
				String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");

				consumer.setTokenWithSecret(token, secret);
				Log.i(TAG, "OAuth - Access Token Retrieved");
				finish();

			} catch (Exception e) {
				Log.e(TAG, "OAuth - Access Token Retrieval Error", e);
			}

			return null;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private class GCMTask extends AsyncTask {

		@Override
		protected Object doInBackground(Object... params) {
			Log.d(TAG, "檢查裝置是否支援 GCM");
			register();
			// 檢查裝置是否支援 GCM
			// GCMRegistrar.checkDevice(MainActivity.this);
			// GCMRegistrar.checkManifest(MainActivity.this);
			// final String regId =
			// GCMRegistrar.getRegistrationId(MainActivity.this);
			// if (regId.equals(""))
			// {
			//
			// Log.d(TAG, "尚未註冊 Google GCM, 進行註冊");

			// register();
			// }
			return null;
		}

	}

	
	
	public void register() {
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		String regId = GCMRegistrar.getRegistrationId(this);
		String SENDER_ID = "617669517466";
		if (regId.equals("")) { // 當發現註冊的id為null的時候，代表還未向GCM註冊，故開始執行註冊程序
			Log.e(TAG, "regId is null. Going to register SENDER_ID to GCM");

			GCMRegistrar.register(this, SENDER_ID);
			gcm = GoogleCloudMessaging.getInstance(this);

			try {
				regId = gcm.register(SENDER_ID);
				registerToServer(this, regId);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// regId = gcm.getRegistrationId(this);
			// registerToServer(this, regId);
			//
			// registerToServer(this, regId , mEmailString, location_lat,
			// location_lng);
		} else {
			registerToServer(this, regId);
		}
	}

	// static void registerToServer(final Context context, final String regId,
	// final String emailString, final double location_lat, final double
	// location_lng) {
	static void registerToServer(final Context context, final String regId) {
		//取得Android id
		String android_id=Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		// 註冊要傳送給server端的訊息
		Map<String, String> params = new HashMap<String, String>();
		params.put("regId", regId);
		// params.put("email", emailString);
		params.put("deviceName", Build.MODEL);
		params.put("androidId", android_id);
		// params.put("location_lat", String.valueOf(location_lat));
		// params.put("location_lng", String.valueOf(location_lng));
		Log.i(TAG, "regId:" + regId+" android_id:"+android_id);
		String time=toolsUtil.getInstance().getNowTime();
		JSONObject object = new JSONObject();
		try {
			object.put("regId", regId);
			object.put("deviceName", Build.MODEL);
			object.put("androidId", android_id);
			//String md5=toolsUtil.getInstance().md5(regId+deviceName+androidId+time);
			object.put("sign", toolsUtil.getInstance().md5(regId+Build.MODEL+android_id+time));
			object.put("time", time);
			HttpClientUtil.getInstance().postJson(gcm_register, object);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		// Log.i(TAG,HttpClientUtil.getInstance().excePost(gcm_register,params));
		// Log.i(TAG, postJson(gcm_register,params));
		GCMRegistrar.setRegisteredOnServer(context, true); // 設定GCM server已註冊
		// long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		//
		// // Once GCM returns a registration id, we need to register it in the
		// // server. As the server might be down, we will retry it a couple
		// // times. (不斷的向server端進行註冊動作，直到成功為止)
		// for (int i = 1; i <= MAX_ATTEMPTS; i++) {
		// Log.d(TAG, "Attempt #" + i + " to register");
		// try {
		// post(serverUrl, params); // 傳送註冊訊息給我自己架設的server端
		// GCMRegistrar.setRegisteredOnServer(context, true); //設定GCM server已註冊
		// mConnectionSuccess = true;
		// return;
		// } catch (IOException e) {
		// // Here we are simplifying and retrying on any error; in a real
		// // application, it should retry only on unrecoverable errors
		// // (like HTTP error code 503).
		// if (i == MAX_ATTEMPTS) {
		// mConnectionSuccess = false;
		// break;
		// }
		// try {
		// Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
		// Thread.sleep(backoff);
		// } catch (InterruptedException e1) {
		// // Activity finished before we complete - exit.
		// Log.d(TAG, "Thread interrupted: abort remaining retries!");
		// Thread.currentThread().interrupt();
		// return;
		// }
		// // increase backoff exponentially
		// backoff *= 2;
		// }
	}

}
